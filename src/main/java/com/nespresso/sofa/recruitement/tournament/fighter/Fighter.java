package com.nespresso.sofa.recruitement.tournament.fighter;

import java.util.ArrayList;
import java.util.List;

import com.nespresso.sofa.recruitement.tournament.weapon.Weapon;
import com.nespresso.sofa.recruitement.tournament.weapon.WeaponFactory;

public abstract class Fighter {
	
	protected Integer points;
	protected List<Weapon> weapons = new ArrayList<Weapon>();

	

	public boolean hasPoints() {
		// TODO Auto-generated method stub
		return points > 0;
	}
	
	public Integer hitPoints() {
		// TODO Auto-generated method stub
		return points;
	}
	
	public void reducePoint(Integer dmg) {
		this.points -= dmg;
		if(points < 0){
			points = 0;
		}
	}
	
	public void fight(Fighter fighter, Integer damage) {
		if(fighter.hasWeapons()){
			damageWeapon();
		}else{
			fighter.reducePoint(damage);
		}
		
	}

	private void damageWeapon() {
		for(Weapon wp : weapons){
			wp.cancelDamage();
		}
		
	}

	private boolean hasWeapons() {
		// TODO Auto-generated method stub
		boolean hasWeapons = false;
		for(Weapon wp : weapons){
			if(wp.isUsefulAgain()){
				hasWeapons = true;
				break;
			}
		}
		return hasWeapons;
	}

	public void equipFighter(String weapon) {
		
		Weapon wp = WeaponFactory.create(weapon);
		weapons.add(wp);
	}
}
