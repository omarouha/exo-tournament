package com.nespresso.sofa.recruitement.tournament.weapon;

public class WeaponFactory {

	public static Weapon create(String weapon) {
		switch (weapon) {
		case "buckler":
			return new Buckler();
// here, we should take into consideration other types : armor, axe ...
		default:
			break;
		}
		return null;
	}

}
