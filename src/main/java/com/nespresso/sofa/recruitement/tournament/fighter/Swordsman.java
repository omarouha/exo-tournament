package com.nespresso.sofa.recruitement.tournament.fighter;


public class Swordsman extends Fighter {
	
	public static Integer HIT_POINTS = 100;
	public static Integer DAMAGE = 5;
	
	
	public Swordsman(){
		this.points = HIT_POINTS;
	}

	public void engage(Viking viking) {

		while (this.hasPoints() && viking.hasPoints()) {
			this.fight(viking, DAMAGE);
			viking.fight(this);
		}
	}
	
	public Swordsman equip(String weapon){
		this.equipFighter(weapon);
		return this;
	}

	


	

}
