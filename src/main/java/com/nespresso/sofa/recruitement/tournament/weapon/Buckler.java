package com.nespresso.sofa.recruitement.tournament.weapon;

public class Buckler extends Weapon {
	
	public static final Integer MAX_BLOCKING_BLOW = 3;

	public Buckler() {
		super();
		name = "buckler";
	}
	
	@Override
	public boolean isUsefulAgain() {
		return numberBlockingBlow < MAX_BLOCKING_BLOW;
	}
	
	

}
