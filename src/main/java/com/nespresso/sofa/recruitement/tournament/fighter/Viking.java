package com.nespresso.sofa.recruitement.tournament.fighter;

public class Viking extends Fighter{

	public static final Integer HIT_POINTS = 120;
	public static final Integer DAMAGE = 6;
	
	public Viking(){
		this.points = HIT_POINTS;
	}
	
	
	public void fight(Fighter swordsman) {
		this.fight(swordsman, DAMAGE);
		
	}
	
	public Viking equip(String weapon) {
		this.equipFighter(weapon);
		return this;
	}
	
	
}
